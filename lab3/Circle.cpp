/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"
//namespace Student1 {
Circle::Circle() {
	r = 0;
}

Circle::Circle(double r) {
	setR(r);
}

void Circle::setR(double r){
	this->r = r;
}

double Circle::getR(){
	return r;
}

double Circle::calculateCircumference(){
	return 2 * PI * r;
}

double Circle::calculateArea(){
	return PI * r * r;
}
//}

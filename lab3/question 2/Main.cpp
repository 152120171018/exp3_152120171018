#include "Triangle.h"
#include "Square.h"
#include "Circle.h"

#include <iostream>
using namespace std;

void test();

/*
 * Question1: Fix the code and compile it */
int main() {
	/****************************************************/
	/*DO NOT CHANGE any line of code in this function */
	Triangle triangle(3, 5, 6);
	triangle.setA(7);
	triangle.setB(8);
	triangle.setC(9);
	double circumference = triangle.calculateCircumference();
	double Circumference = triangle.calculateCircumference();
	cout << "Triangle Circumference => " << Circumference << endl << endl;

	Circle circle(3);
	double circleCircumference1 = circle.calculateCircumference();
	cout << "Circle Circumference1 => " << circleCircumference1 << endl;
	circle.setR(5);
	double circleCircumference2 = circle.calculateCircumference();
	cout << "Circle Circumference2 => " << circleCircumference2 << endl;
	double circleArea = circle.calculateArea();
	cout << "Circle Area => " << circleArea << endl << endl;

	Square square(3);
	double squareCircumference1 = square.calculateCircumference();
	cout << "Square Circumference 1 (equilateral a*4)=> " << squareCircumference1 << endl;
	square.setA(4);
	square.setB(5);
	double squareCircumference2 = square.calculateCircumference();
	cout << "Square Circumference 2 (scalene (a+b)*2)=> " << squareCircumference2 << endl;
	double squareArea = square.calculateArea();
	/****************************************************/
	test();
	return 0;
}

void test() {
	const Circle circle1(30);
	/* Question2: Block the changing r of above object not other objects only above object.*/
	circle1.setR(20); //This line must show compile error.
	double circumference = circle1.calculateCircumference();
	circle1.getR();

	/*DO NOT REMOVE below code block*/
	{
		Circle circle2(30);
		circle2.setR(20);
		double circumference2 = circle2.calculateCircumference();
		circle2.getR();
	}
	/*End of code block*/

	Circle circle4(5);
	circle4.setR(10);
	Circle circle5(10);
	/* Question3: In Circle class, create an equals function which returns boolean to compare circle4 and circle5 objects
	 * and print if they are equal or not. */

	/* Question4: Review the PI variable and make it unchangeable from anywhere of that program.
	 * You know PI is always 22/7 */

	/* Question5: Overload the setR method of Circle class to take integer values */

}

/*
 * Question6: Review the code and fix the bugs
 */


/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

using namespace std;
#include <iostream>
#include <cmath>

class Circle {
public:
	Circle();
	Circle(double);
	~Circle();


	void setR(double);
	void setR(int);
	double getR() const;


	double calculateCircumference() const;
	double calculateArea();
	bool CircleisEqual(const Circle& c1, Circle& c2);


private:
	double r;
	const double PI = 3.14;
};
#endif /* CIRCLE_H_ */

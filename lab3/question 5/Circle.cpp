/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Circle.h"

//namespace Student1 {
Circle::Circle() {
	r = 0;
}

Circle::Circle(double r) {
	setR(r);
}
Circle::~Circle() {

}
/*
	Question 5: Overload the setR method. I added this question to git by mistake first. 
*/
void Circle::setR(double r) {
	if (r < 0) cout << "r must be greater than 0" << endl;
	else this->r = r;
}

void Circle::setR(int r) {
	if (r < 0) cout << "r must be greater than 0" << endl;
	else this->r = r;
}

double Circle::getR() const {
	return r;
}

double Circle::calculateCircumference() const {
	return 2 * PI * r;
}

double Circle::calculateArea() {
	return PI * r * r;
}

bool Circle::CircleisEqual(const Circle& circle1, Circle& circle2) {
	if (circle1.r == circle2.r) return 1;
	else return 0;
}
//}
